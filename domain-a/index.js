function loadXml() {

  $.ajax({
      url: "http://domain_b.com/resource.xml",
      dataType: "xml",
      success: function(data) {
        console.log(data);

        var oSerializer = new XMLSerializer();
        var sXML = oSerializer.serializeToString(data);
        document.getElementById('container').innerText = sXML;
      }
  });

  console.log("Loaded!");

}
