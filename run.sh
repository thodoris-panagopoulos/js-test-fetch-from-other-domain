#!/bin/bash

# Run main portal - domain-a

  docker run --name domain-a -p 81:80 -v $PWD/domain-a:/usr/share/nginx/html -d nginx

# Run resource - domain-b

  docker run --name domain-b -p 82:80 -v $PWD/domain-b:/usr/share/nginx/html -d nginx

# Run haproxy

  docker run -it --rm haproxy haproxy -v $PWD/haproxy/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg -c -f /usr/local/etc/haproxy/haproxy.cfg

  docker run --name haproxy --network host -v $PWD/haproxy/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg -v $PWD/haproxy/domain_a.pem:/etc/ssl/certs/cert.pem -d haproxy

# Generate SSL certificate

  export DOMAIN=domain_a
  openssl genrsa -out $DOMAIN.key 2048
  sudo openssl req -new -key $DOMAIN.key -out domain_a.csr
  openssl x509 -req -days 365 -in $DOMAIN.csr -signkey $DOMAIN.key -out $DOMAIN.crt
  sudo bash -c 'cat $DOMAIN.key $DOMAIN.crt >> $DOMAIN.pem'
